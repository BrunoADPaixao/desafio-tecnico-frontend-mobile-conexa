import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, Image, TouchableOpacity, Modal, TouchableHighlight, Alert } from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import DateTimePicker from '@react-native-community/datetimepicker';
import { format } from 'date-fns';

import FormInput from '../components/InputComponent';
import { server, showError } from '../common'; 
import commonStyles from '../commonStyles';
import ImgDoctor from '../assets/img/doctor.jpg';

const initialState = {
  apointments: [],
  patient: '',
  observation: '',
}

// const dateToString = formatWithOptions({ locale: eo }, 'D MMMM YYYY')

function getDate(date) {
  let newString = date.split(' ');
  return newString[0];
}

function getHour(date) {
  let newString = date.split(' ');
  return newString[1];
}

export default class Home extends Component {
  state = {
    ...initialState,
    modalVisible: false,
    date:  new Date(1598025600000),
    mode:  'date',
    show: false,
    showDate: 'Escolher data',
    showTime: 'Escolher hora'
  }
  
  componentDidMount = async () => {
    this.loadAgenda();
  }

  loadAgenda = async () => {
    const userDataJson = await AsyncStorage.getItem('userData');
    let userData = null;

    try {
      userData = JSON.parse(userDataJson);
    } catch(e) {
      // userData está inválido
    }

    try {
      const res = await axios.get(`${server}/consultas`, { 
        headers: { 'Authorization' : `Bearer ${userData.token}` } 
      });

      this.setState({ apointments: res.data.data });
    } catch(e) {
      showError(e);
    }
  }

  

  pushApointiment = async () => {
    try {
      const res = await axios.post(`${server}/consulta`, {
        idMedico: '1',
        observacao: this.state.observation,
        dataConsulta: format(this.state.date, 'dd/MM/yyyy kk:mm'),
        paciente: this.state.patient
      });
      Alert.alert('Sucesso', 'Consulta agendada!');
    } catch (e) {
      showError(e);
    }
  }

  navigateToDetail(apointment) {
    this.props.navigation.navigate('ApointmentDetail', { apointment });
  }

  render() {
    const onChange = (event, selectedDate) => {
      const currentDate = selectedDate || date;
      this.setState({ show: Platform.OS === 'ios' });
      this.setState({ date: currentDate });
    
      this.setState({ showDate: format(this.state.date, 'dd/MM/yyy')});
      this.setState({ showTime: format(this.state.date, 'kk:mm')});
    };
    
    const showMode = currentMode => {
      this.setState({ show: true });
      this.setState({ mode: currentMode });
    };
    
    const showDatepicker = () => {
      showMode('date');
    };
    
    const showTimepicker = () => {
      showMode('time');
    };

    const setModalVisible = (visible) => {
      this.setState({modalVisible: visible});
    }

  const Item = ({ doctor, patient, apointmentDate, apointmentHour }) => (
      <View style={styles.item}>
        <View style={styles.detailContainer}>
          <Icon name='user' size={20} style={commonStyles.icon} />
          <Text style={styles.patientName}>Paciente: {patient}</Text>
        </View>
        <View style={styles.detailContainer}>
          <Icon name='user-md' size={20} style={commonStyles.icon} />
          <Text style={commonStyles.aditionalInfo}>Médico: {doctor}</Text>
        </View>
        <View style={styles.detailContainer}>
          <Icon name='calendar' size={20} style={commonStyles.icon} />
          <Text style={commonStyles.aditionalInfo}>Data: {apointmentDate}</Text>
        </View>
        <View style={styles.detailContainer}>
          <Icon name='history' size={20} style={commonStyles.icon} />
          <Text style={commonStyles.aditionalInfo}>Horário: {apointmentHour}</Text>
        </View>
      </View>
  );

    const renderItem = ({ item }) => (
      <TouchableOpacity onPress={() => this.navigateToDetail(item)}>
        <Item doctor={item.medico.nome} patient={item.paciente} apointmentDate={getDate(item.dataConsulta)} apointmentHour={getHour(item.dataConsulta)} />
      </TouchableOpacity>
    );
  
    return (
      <SafeAreaView style={styles.container}>
        <View style={commonStyles.profileContainer}>
          <Image style={commonStyles.profileImg} source={ImgDoctor}  />
          <View style={commonStyles.infoContainer}>
            <Text style={commonStyles.profileName}>Dr. Daniel Vieira</Text>
            <Text style={commonStyles.aditionalInfo}>Clínico Geral</Text>
          </View>
        </View>
        <FlatList
          data={this.state.apointments}
          renderItem={renderItem}
          keyExtractor={item => item.id.toString()}
        />
        <TouchableHighlight 
          onPress={() => { setModalVisible(true); }} style={commonStyles.iconAddWrapper}>
          <Icon name='plus' size={25} style={commonStyles.iconAdd} />
        </TouchableHighlight>
        <View style={styles.centeredView}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.modalVisible}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView} onpr>
                <TouchableOpacity style={styles.btnDismiss} onPress={() => setModalVisible(false)}>
                  <Icon name='close' size={20} style={commonStyles.iconClose} />
                </TouchableOpacity>
                <View style={styles.positioning}>
                  <FormInput icon='user' placeholder='Nome do paciente' 
                    value={this.state.patient}
                    style={styles.input} 
                    onChangeText={patient => this.setState({ patient })} />
                </View>
                <View style={styles.positioning}>
                  <FormInput rows={5} icon='file' placeholder='Observações' 
                    value={this.state.observation}
                    style={styles.input} 
                    onChangeText={observation => this.setState({ observation })} />
                </View>
                <View style={[styles.positioning, styles.datePickerContainer]}>
                  <TouchableHighlight style={[styles.btnPicker, styles.btnPickerLeft]} onPress={showDatepicker}>
                    <Text style={styles.textStyle}>{this.state.showDate}</Text>
                  </TouchableHighlight>
                  <TouchableHighlight style={[styles.btnPicker, styles.btnPickerRight]} onPress={showTimepicker}>
                    <Text style={styles.textStyle}>{this.state.showTime}</Text>
                  </TouchableHighlight>
                </View>
                {this.state.show && (
                  <DateTimePicker
                    testID="dateTimePicker"
                    value={this.state.date}
                    mode={this.state.mode}
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                  />
                )}
                <TouchableHighlight
                  style={styles.openButton}
                  onPress={() => {
                    setModalVisible(false);
                  }}
                >
                  <Text style={styles.textStyle} onPress={() => this.pushApointiment()}>Agendar</Text>
                </TouchableHighlight>
              </View>
            </View>
          </Modal>
        </View> 
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative'
  },
  item: {
    // backgroundColor: '#f9c2ff',
    borderColor: '#D5D5D5',
    borderWidth: 1,
    width: 350,
    paddingVertical: 10,
    paddingHorizontal: 20, 
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 8
  },
  patientName: {
    fontSize: 18,
    color: '#3D3D3D'
  },
  doctor: {
    color: '#000',
    fontSize: 40
  },
  detailContainer: {
    flexDirection: 'row',
    borderBottomColor: '#D5D5D5',
    paddingVertical: 10,
    alignItems: 'center',
  },
  centeredView: {
    // flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    position: 'relative',
    margin: 20,
    marginTop: 200,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#19769F",
    borderRadius: 5,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  positioning: {
    marginBottom: 10
  },
  datePickerContainer: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  btnPicker: {
    borderRadius: 5,
    backgroundColor: '#19769F',
    padding: 10,
  },
  btnPickerLeft: {
    marginRight: 20,
  },
  btnPickerRight: {
    marginLeft: 20,
  },
  btnDismiss: {
    position: 'absolute',
    top: 10,
    right: 0,
    backgroundColor: '#FFF'
  }
});