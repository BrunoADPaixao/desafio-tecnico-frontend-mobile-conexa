
import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const AppStack = createStackNavigator();

import Auth from './screens/Auth';
import Home from './screens/Home';
import ApointmentDetail from './screens/ApointmentDetail';
import AuthOrApp from './screens/AuthOrApp';

export default function Routes() {
  return (
    <NavigationContainer>
      <AppStack.Navigator>
        <AppStack.Screen name='Auth' component={Auth} />
        <AppStack.Screen name='AuthOrApp' component={AuthOrApp} />
        <AppStack.Screen name='Home' component={Home} />
        <AppStack.Screen name='ApointmentDetail' component={ApointmentDetail} />
      </AppStack.Navigator>
    </NavigationContainer>
  );
}