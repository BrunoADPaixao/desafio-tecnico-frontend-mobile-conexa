import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { 
        Text, StyleSheet, View,
        TouchableOpacity, Platform, Image,} from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { LinearGradient } from 'expo-linear-gradient';

import AuthInput from '../components/InputComponent';
import { server, showError, showSuccess } from '../common';
import commonStyles from '../commonStyles';
import logoConexa from '../assets/img/logo-conexa.png'

const initialState = {
  name: '',
  email: 'string',
  password: 'string',
  confirmPassword: '',
  stageNew: false
}

export default class Auth extends Component {
  state = {
    ...initialState
  }

  signinOrSignup = () => {
    // this.props.navigation.navigate('Home');
    if (this.state.stageNew) {
      this.signup();
    } else {
      this.signin();
    }
  }
  
  signup = async () => {
    try {
      await axios.post(`${server}/registrar`, {
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
        confirmPassword: this.state.confirmPassword,
      });

      showSuccess('Usuário cadastrado!');
      this.setState({ ...initialState });
    } catch (e) {
      showError(e);
    }
  }

  signin = async () => {
    try {
      const res = await axios.post(`${server}/login`, {
        email: this.state.email,
        password: this.state.password
      });

      AsyncStorage.setItem('userData', JSON.stringify(res.data.data));
      axios.defaults.headers.common['Authorization'] = `Bearer ${res.data.data.token}`;
      this.props.navigation.navigate('Home', res.data.data);
    } catch (e) {
      showError(e);
    }
  }
  
  render() {
    
    const validations = [];
    validations.push(this.state.email);
    validations.push(this.state.password);

    if (this.state.stageNew) {
      validations.push(this.state.name);
      validations.push(this.state.password === this.state.confirmPassword)
    }

    const validForm = validations.reduce((t, a) => t && a);
    
    return (
      <View style={styles.container}>
        <Image style={styles.imgLogo} source={logoConexa}  />
        <Text style={styles.title}>{this.state.stageNew ? 'Registrar' : 'Logar'}</Text>
        <View style={styles.formContainer}>
          {this.state.stageNew &&
            <AuthInput icon='user' placeholder='Nome' 
            value={this.state.name}
            style={styles.input} 
            onChangeText={name => this.setState({ name })} />
          }
          <AuthInput icon='at' placeholder='E-mail' 
            value={this.state.email}
            style={styles.input} 
            onChangeText={email => this.setState({ email })} />
          <AuthInput icon='lock' placeholder='Senha' 
          value={this.state.password}
            style={styles.input}
            secureTextEntry={true}
            onChangeText={password => this.setState({ password })} />
          {this.state.stageNew &&
            <AuthInput icon='asterisk' placeholder='Confirmação de senha' 
            value={this.state.confirmPassword}
            style={styles.input} 
            secureTextEntry={true}
            onChangeText={confirmPassword => this.setState({ confirmPassword })} />
          }
          <TouchableOpacity onPress={this.signinOrSignup}
          disabled={!validForm} >
              <LinearGradient onPress={this.signinOrSignup} colors={['#19769F', '#35D8A6']} style={styles.button}>
                <Text
                  style={styles.buttonText}>
                  {this.state.stageNew ? 'Registrar' : 'Entrar'}
                </Text>
              </LinearGradient>
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={{ padding: 10 }}
          onPress={() => this.setState({ stageNew: !this.state.stageNew })}>
          <Text style={styles.defaultText}>
          {this.state.stageNew ? 'Já possui conta?' : 'Ainda não possui conta?'}
          </Text>
        </TouchableOpacity>
    
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFF'
  },
  imgLogo: {
    flex: 1,
    width: 100
  },
  title: {
    color: '#19769F',
    textAlign: 'left',
    fontSize: 30,
    width: '80%',
    marginBottom: 10,
  },
  formContainer: {
    padding: 20,
    width: '90%'
  },
  input: {
    marginTop: 10,
    backgroundColor: '#FFF',
    borderColor: '#D5D5D5',
    borderWidth: 1,
    height: 40,
    padding: Platform.OS == 'ios' ? 15: 10,
  },
  button: {
    backgroundColor: '#080',
    marginTop: 10,
    padding: 10,
    alignItems: 'center',
    borderRadius: 5
  },
  buttonText: {
     color: '#FFF',
     fontSize: 18,
     fontWeight: 'bold',
  },
  defaultText: {
    color: '#95989A',
    fontSize: 16
  },
  imgLogo: {
    position: 'absolute',
    top: 20,
    left: 35,
    width: 136,
    height: 26
  }
})