import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  profileContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20
  },
  infoContainer: {
    justifyContent: 'center'
  },
  profileImg: {
    width: 120,
    height: 120,
    borderRadius: 60,
    marginRight: 20
  },
  profileName: {
    fontSize: 28,
    color: '#19769F'
  },
  aditionalInfo: {
    fontSize: 16,
    color: '#51565F',
  },
  icon: {
    color: '#35D8A6',
    marginRight: 15
  },
  iconAdd: {
    color: '#35D8A6',
    paddingVertical: 10,
    paddingHorizontal: 13
  },
  iconAddWrapper: {
    position: 'absolute',
    bottom: 25,
    right: 25,
    backgroundColor: '#19769F',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconClose: {
    color: '#19769F',
    marginRight: 15
  }
});