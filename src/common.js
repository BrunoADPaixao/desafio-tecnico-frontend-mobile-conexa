import { Alert, Platform } from 'react-native';
import axios from 'axios'

const server = 'http://desafio.conexasaude.com.br/api';

function showError(err) {
  Alert.alert('Ops! Ocorreu um problema', `Mensagem: ${err}`);
}

function showSuccess(msg) {
  Alert.alert('Sucesso', msg);
}

export { server, showError, showSuccess }