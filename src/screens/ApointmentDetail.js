import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { useNavigation, useRoute} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome';

import commonStyles from '../commonStyles';
import ImgPatient from '../assets/img/patient.jpg';

export default function ApointmentDetail() {

  const route = useRoute();

  const apointment =  route.params.apointment;
  
  return (
    <View style={styles.container}>
      <View style={commonStyles.profileContainer}>
        <Image style={commonStyles.profileImg} source={ImgPatient}  />
        <View style={commonStyles.infoContainer}>
          <Text style={commonStyles.profileName}>{apointment.paciente}</Text>
          <Text style={commonStyles.aditionalInfo}>Idade: 25 anos</Text>
          <Text style={commonStyles.aditionalInfo}>Peso: 75Kg</Text>
        </View>
      </View>
      <View style={styles.apointment}>
        <View style={styles.detailContainer}>
          <Icon name='user-md' size={20} style={commonStyles.icon} />
          <Text style={styles.detail}>Médico: {apointment.medico.nome}</Text>
        </View>
        <View style={styles.detailContainer}>
          <Icon name='calendar' size={20} style={commonStyles.icon} />
          <Text style={styles.detail}>{apointment.dataConsulta}</Text>
        </View>
        <View style={styles.detailContainerLast}>
          <Icon name='file' size={20} style={commonStyles.icon} />
          <Text style={styles.detail}>{apointment.observacao}</Text>
        </View>
      </View>
      <TouchableOpacity style={styles.btnCancel}>
        <Text style={styles.btnText}>Cancelar Consulta</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 25
  },
  apointment: {
    marginTop: 40,
    justifyContent: 'center',
    backgroundColor: '#FFF',
    borderRadius: 4,
    padding: 20,
    borderWidth: 1,
    height: 250,
    width: '100%',
    borderColor: '#5FE5BC'
  },
  detailContainer: {
    flexDirection: 'row',
    borderBottomColor: '#D5D5D5',
    borderBottomWidth: 1,
    paddingVertical: 20
  },
  detailContainerLast: {
    flexDirection: 'row',
    borderBottomColor: '#D5D5D5',
    borderBottomWidth: 0,
    paddingVertical: 25
  },
  detail: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#616161'
  },
  btnCancel: {
    marginTop: 15,
    padding: 15,
    width: '80%',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#A8A8A8',
  },
  btnText: {
    fontSize: 16,
    textAlign: 'center',
    color: '#A8A8A8',
    fontWeight: 'bold'
  }
});